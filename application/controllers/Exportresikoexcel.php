<?php
// require_once('vendor/autoload.php');
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


defined('BASEPATH') or exit('No direct script access allowed');

class Exportresikoexcel extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Modelcetak', 'cetak');
    }
   public function cetak_resiko_all()
    {
       $data = $this->cetak->getresikoall();
    //    var_dump($data);
    //    die();

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'DATA RISIKO');
        $sheet->setCellValue('A3',  $data[0]['nama_pemkot']);
        // $sheet->setCellValue('A4', $this->session->userdata('name'));


        $sheet->setCellValue('A5', 'NO');
        $sheet->setCellValue('B5', 'OPD');
        $sheet->setCellValue('C5', 'PROGRAM');
        $sheet->setCellValue('D5', 'KEGIATAN');
        $sheet->setCellValue('E5', 'RISIKO');
        $sheet->setCellValue('F5', 'SEBAB');
        $sheet->setCellValue('G5', 'DAMPAK');
        $sheet->setCellValue('H5', 'SKOR KEMUNGKINAN');
        $sheet->setCellValue('I5', 'SKOR DAMPAK');
        $sheet->setCellValue('J5', 'SKOR RISIKO');
        $no = 1;
        $x = 6;

        foreach ($data as $d) {
            # code...
            $sheet->setCellValue('A' . $x, $no++);
            $sheet->setCellValue('B' . $x, $d['name']);
            $sheet->setCellValue('C' . $x, $d['program']);
            $sheet->setCellValue('D' . $x, $d['kegiatan']);
            $sheet->setCellValue('E' . $x, $d['resiko']);
            $sheet->setCellValue('F' . $x, $d['sebab']);
            $sheet->setCellValue('G' . $x, $d['dampak']);
            $sheet->setCellValue('H' . $x, $d['n_kemungkinan']);
            $sheet->setCellValue('I' . $x, $d['n_dampak']);
            $sheet->setCellValue('J' . $x, $d['n_resiko']);
            $x++;
        }

        $styleArray = [
            'borders' => [
                'allBorders' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
        ];
        $y = $x - 1;
        $sheet->getStyle('A5:J' . $y)->applyFromArray($styleArray);

        $writer = new Xlsx($spreadsheet);
        $filename = 'report_risiko_' . $this->session->name;

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '.xlsx"');
        header('Cache-Control: max-age=0');

        $writer->save('php://output');

        //tambah komen
    
    }
}