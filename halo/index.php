
<!DOCTYPE html>
<html lang="en-US">
<head>
							<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="profile" href="http://gmpg.org/xfn/11">
		<link rel="pingback" href="https://tes.bpkp.pw/xmlrpc.php">
		<!-- Start Favicon -->
									<link rel="icon" type="image/png" href="">
				<!-- End Favicon -->
		<title>Saluran Komunikasi BPKP Provinsi Sumatera Utara</title>
		<meta name="description" content="Saluran Komunikasi BPKP Provinsi Sumatera Utara">
		<meta name="robots" content="index, follow" />
		<meta name="googlebot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
		<meta name="bingbot" content="index, follow, max-snippet:-1, max-image-preview:large, max-video-preview:-1" />
		<link rel='stylesheet' id='mylinks-public-css-css'  href='https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/css/wp-mylinks-public.min.css?ver=6.0.2' media='all' />
<link rel='stylesheet' id='mylinks-youtube-css-css'  href='https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/css/wp-mylinks-youtube.min.css?ver=6.0.2' media='all' />
<style type="text/css">.youtube-player .play{ background: url(https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/images/play.png) no-repeat;}</style>				<style type="text/css">.mylinks .avatar{background:#fdf497;background:radial-gradient(circle at 30% 107%,#fdf497 0,#fdf497 5%,#fd5949 45%,#d6249f 60%,#8a3fb6 90%);background:radial-gradient(circle at 30% 107%,#fdf497 0,#fdf497 5%,#fd5949 45%,#d6249f 60%,#8a3fb6 90%);background:radial-gradient(circle at 30% 107%,#fdf497 0,#fdf497 5%,#fd5949 45%,#d6249f 60%,#8a3fb6 90%)}</style></head>
<body class="mylinks-body default">			<div class="mylinks">
			<div class="avatar">
													<img width="140" height="140" src="https://tes.bpkp.pw/wp-content/uploads/2022/04/BPKP_Logo_2x1.png" alt="Halo BPKP Sumut">
							</div>
			<div class="name">
				Halo BPKP Sumut			</div>
			<div class="description">
				Saluran Komunikasi Publik<br>
Perwakilan BPKP Provinsi Sumatera Utara<br>
Jl. Jenderal Gatot Subroto KM 5,5 Medan<br>			</div>
							<div class="user-profile">
										<!-- Start Facebook -->
															<!-- End Facebook -->
					<!-- Start Twitter -->
															<!-- End Twitter -->
					<!-- Start Linkedin -->
															<!-- End Linkedin -->
					<!-- Start Instagram -->
											<a href="https://instagram.com/bpkpsumut1" target="_blank" class="user-profile-link" rel="noopener noreferrer nofollow">
															<img align="middle" class="mylinks-social-icons" width="32" height="32" src="https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/images/instagram.png">
													</a>
										<!-- End Instagram -->
					<!-- Start Youtube -->
											<a href="https://www.youtube.com/channel/UC7ZC9SXMtBgR2pX27zmJ5Uw" target="_blank" class="user-profile-link" rel="noopener noreferrer nofollow">
															<img align="middle" class="mylinks-social-icons" width="32" height="32" src="https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/images/youtube.png">
													</a>
										<!-- End Youtube -->
					<!-- Start Pinterest -->
															<!-- End Pinterest -->
					<!-- Start TikTok -->
															<!-- End TikTok -->
				</div>
						<!-- End Top Social Media -->
			<div class="links">
																			<div class="link">
							<a id="link_count" class="button link-with-image inline-photo show-on-scroll" href="http://wa.me/6281376838263?text=Yth.%20BPKP%20Sumut,%20Saya%20Nama:%20%5B%20...%20%5D%20NIP:%20%5B...%5D%20Jabatan:%20%5B...%5D%20Instansi:%20%5B...%5D%20bermaksud%20konsultasi%20tentang%5B...%5D" target="_blank" rel="noopener">
								<div class="thumbnail-wrap">
									<img src="https://tes.bpkp.pw/wp-content/uploads/2022/04/wa.jpg" class="link-image" alt="thumbnail">
								</div>
								<span class="link-text">Konsultasi Teknis</span>
							</a>
						</div>
																				<div class="link">
							<a id="link_count" class="button link-with-image inline-photo show-on-scroll" href="https://www.bpkp.go.id/sumut.bpkp" target="_blank" rel="noopener">
								<div class="thumbnail-wrap">
									<img src="https://tes.bpkp.pw/wp-content/uploads/2022/04/images.jpg" class="link-image" alt="thumbnail">
								</div>
								<span class="link-text">Website Resmi</span>
							</a>
						</div>
																				<div class="link">
							<a id="link_count" class="button link-with-image inline-photo show-on-scroll" href="https://wa.me/6281376838263?text=Yth.%20BPKP%20Sumut,Saya%20%5B...%5D%20wartawan%20dari%20media%20%5B...%5D%20" target="_blank" rel="noopener">
								<div class="thumbnail-wrap">
									<img src="https://tes.bpkp.pw/wp-content/uploads/2022/04/download.png" class="link-image" alt="thumbnail">
								</div>
								<span class="link-text">Permohonan Berita dan Press Release</span>
							</a>
						</div>
																				<div class="link">
							<a id="link_count" class="button link-with-image inline-photo show-on-scroll" href="http://wbs.bpkp.go.id/wbs/" target="_blank" rel="noopener">
								<div class="thumbnail-wrap">
									<img src="https://tes.bpkp.pw/wp-content/uploads/2022/04/whistle-icon-referee-symbol-flat-vector-5402121-e1649491054496.jpg" class="link-image" alt="thumbnail">
								</div>
								<span class="link-text">Whistleblowing</span>
							</a>
						</div>
																				<div class="link">
							<a id="link_count" class="button link-with-image inline-photo show-on-scroll" href="https://goo.gl/maps/KPJGfcrRnuCVgVNi7" target="_blank" rel="noopener">
								<div class="thumbnail-wrap">
									<img src="https://tes.bpkp.pw/wp-content/uploads/2022/04/download-1.png" class="link-image" alt="thumbnail">
								</div>
								<span class="link-text">Alamat (Google Map)</span>
							</a>
						</div>
												</div>
		</div>
			<!-- End Top Social Media -->
<footer id="site-footer" role="contentinfo" class="mylinks-footer">
			</footer>
<script src='https://tes.bpkp.pw/wp-content/plugins/wp-mylinks/public/js/wp-mylinks-public.js?ver=6.0.2' id='mylinks-public-js-js'></script>
</body>
</html>