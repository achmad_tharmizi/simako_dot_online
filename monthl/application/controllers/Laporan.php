<?php

// require './libraries/REST_Controller.php';
use chriskacerguis\RestServer\RestController;

defined('BASEPATH') or exit('No direct script access allowed');

class Laporan extends RestController
{

    function __construct()
    {
        // parent::__construct($config);
        parent::__construct();
        $this->load->database();
    }

    // Menampilkan data pegawai
    function index_get()
    {
        $id_pegawai = $this->get('id_pegawai');
       
        if ($id_pegawai == '' ) {
            $this->response([
                'status' => FALSE,
                'message' => 'id pegawai kosong'
            ], RestController::HTTP_NOT_FOUND);
        } else {

            $sqlRekap = "SELECT 
            CASE WHEN periode_bulan=1 THEN 'Januari'
            WHEN periode_bulan=2 THEN 'Februari' 
            WHEN periode_bulan=3 THEN 'Maret' 
            WHEN periode_bulan=4 THEN 'April' 
            WHEN periode_bulan=5 THEN 'Mei' 
            WHEN periode_bulan=6 THEN 'Juni' 
            WHEN periode_bulan=7 THEN 'Juli' 
            WHEN periode_bulan=8 THEN 'Agustus' 
            WHEN periode_bulan=9 THEN 'September'
            WHEN periode_bulan=10 THEN 'Oktober'
            WHEN periode_bulan=11 THEN 'November'
            WHEN periode_bulan=12 THEN 'Desember'  
            ELSE 'Salah Bulan' END AS bulan_nama,
            periode_bulan,count(periode_minggu) as minggu 
            FROM laporan_mingguan 
            WHERE id_pegawai = '".$id_pegawai."' AND is_deleted = 0
            GROUP BY periode_bulan
            ORDER BY periode_bulan asc";
            $laporanRekap = $this->db->query($sqlRekap)->result();

            $sqlRinci = "SELECT a.*,
            CASE WHEN periode_bulan=1 THEN 'Januari'
            WHEN periode_bulan=2 THEN 'Februari' 
            WHEN periode_bulan=3 THEN 'Maret' 
            WHEN periode_bulan=4 THEN 'April' 
            WHEN periode_bulan=5 THEN 'Mei' 
            WHEN periode_bulan=6 THEN 'Juni' 
            WHEN periode_bulan=7 THEN 'Juli' 
            WHEN periode_bulan=8 THEN 'Agustus' 
            WHEN periode_bulan=9 THEN 'September'
            WHEN periode_bulan=10 THEN 'Oktober'
            WHEN periode_bulan=11 THEN 'November'
            WHEN periode_bulan=12 THEN 'Desember'  
            ELSE 'Salah Bulan' END AS bulan_nama,
            CASE WHEN status_laporan=0 THEN 'Diperiksa'
            WHEN status_laporan=1 THEN 'Disetujui'
            WHEN status_laporan=2 THEN 'Ditolak' 
            ELSE 'Tidak Diketahui' END AS ket_status,
            b.id_jabatan
            FROM laporan_mingguan a
            JOIN pegawai b on b.id=a.id_pegawai 
            WHERE a.id_pegawai = '".$id_pegawai."' AND a.is_deleted = 0
            ORDER BY periode_bulan,periode_minggu ASC";
            $laporanRinci = $this->db->query($sqlRinci)->result();
            
            // var_dump($laporanRekap);die();
             $this->response([
                    'status' => TRUE,
                    'data' => array(
                        'rekap' => $laporanRekap,
                        'rinci' => $laporanRinci
                    )
                ], RestController::HTTP_OK);
         
    }
    }
    function index_post() {
        $data = array(
                    // 'id'           => $this->post('id'),
                    'id_pegawai'=> $this->post('id_pegawai'),
                    'periode_bulan'  => $this->post('periode_bulan'),
                    'periode_minggu'    => $this->post('periode_minggu'), 
                    'kebersihan'    => $this->post('kebersihan'), 
                    'kehadiran'    => $this->post('kehadiran'), 
                    'kerapian'    => $this->post('kerapian'), 
                    'created_by' => $this->post('created_by'),
                    
                );
        // $insert = $this->db->insert('laporan_mingguan', $data);
        
        $sqlCheckDouble ="SELECT * 
        From laporan_mingguan 
        Where id_pegawai = '".$data['id_pegawai']."' AND periode_bulan = '".$data['periode_bulan']."' AND 
        periode_minggu = '".$data['periode_minggu']."' AND is_deleted =0";
        $check = $this->db->query($sqlCheckDouble)->result();

       if ($check == "" || $check == null) {
         $sql = "INSERT INTO laporan_mingguan (id_pegawai,periode_bulan,periode_minggu,kebersihan,kehadiran,kerapian,created_by)
        VALUES ('".$data['id_pegawai']."','".$data['periode_bulan']."','".$data['periode_minggu']."','".$data['kebersihan']."',
        '".$data['kehadiran']."','".$data['kerapian']."','".$data['created_by']."')";
        $insert = $this->db->query($sql);


        if ($insert) {
            $this->response([
                'status' => TRUE,
                'data' => $data, 
                200]);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
        
       }else{
         $this->response([
            'status' => FALSE, 
            'message' => 'Data Sudah Ada Atau Belum Input body', 
            'status Kode' => RestController::HTTP_NOT_FOUND
         ]);
       }

       
    }

    function index_put() {
        $data = array(
                    'id_laporan' => $this->put('id_laporan'),
                    'status_laporan' => $this->put('status_laporan'),
                    'catatan' => $this->put('catatan'),
                    'updated_by' => $this->put('updated_by')
                );
                
        $catatan="";
        if ($data['catatan'] != "") {
                $catatan = ",catatan='".$data['catatan']."'";
            }

        $sql="UPDATE laporan_mingguan SET status_laporan='".$data['status_laporan']."',updated_by='".$data['updated_by']."'
        ,updated_at=current_timestamp()".$catatan."
        WHERE id_laporan='".$data['id_laporan']."'";
        $update = $this->db->query($sql);
        
         if ($update) {
            $this->response([
                'status' => TRUE,
                'data' => $update, 
                'status code'=>200]);
        } else {
            $this->response(array('status' => 'fail', 502));
        }
        
    //    }else{
    //      $this->response([
    //         'status' => FALSE, 
    //         'message' => 'Data Sudah Ada Atau Belum Input body', 
    //         'status Kode' => RestController::HTTP_NOT_FOUND
    //      ]);
    //    }
    }
    //Masukan function selanjutnya disini
}