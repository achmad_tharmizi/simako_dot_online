<?php

// require './libraries/RestController.php';
use chriskacerguis\RestServer\RestController;

defined('BASEPATH') OR exit('No direct script access allowed');

class PegawaiByKoordinator extends RestController {

	 function __construct() {
        // parent::__construct($config);
        parent::__construct();
        $this->load->database();
    }

    // Menampilkan data pegawai
    function index_get() {
            $id_jabatan = $this->get('id_jabatan');
            $where='';
            if($id_jabatan != 10 && $id_jabatan != '') {
                $where = "AND koordinator = ".$id_jabatan;
            }
            $sql = "SELECT * FROM (SELECT a.id,a.nama_pegawai,a.id_jabatan,b.nama_jabatan,b.koordinator,a.password,a.is_deleted
            FROM pegawai a JOIN jabatan b ON a.id_jabatan = b.id) AS c
            WHERE is_deleted=0 ".$where."
            ORDER BY id_jabatan,nama_pegawai";
            $pegawai = $this->db->query($sql)->result();
        
             if ($id_jabatan == '' ) {
            $this->response([
                'status' => FALSE,
                'message' => 'id_jabatan kosong'
            ], RestController::HTTP_NOT_FOUND);
        } else {
             $this->response([
                    'status' => TRUE,
                    'data' => $pegawai
                ], RestController::HTTP_OK);
         
        }

    }
    
}