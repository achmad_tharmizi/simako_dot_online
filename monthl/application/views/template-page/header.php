<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Monokomp | <?= $judul ?></title>

    <!-- CSS -->
    <link rel="icon" href="<?=base_url()?>assets-lte3/img/icon.png">
    <!-- Google Font: Source Sans Pro -->
    <!-- <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback"> -->
    <!-- Font Awesome -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/fontawesome-free/css/all.min.css">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <!-- Tempusdominus Bootstrap 4 -->
    <link rel="stylesheet"
        href="<?=base_url()?>assets-lte3/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <!-- JQVMap -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/jqvmap/jqvmap.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/dist/css/adminlte.min.css">
    <!-- overlayScrollbars -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/daterangepicker/daterangepicker.css">
    <!-- summernote -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/summernote/summernote-bs4.min.css">
    <!-- Char.js -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/chart.js/Chart.min.css">
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet"
        href="<?=base_url()?>assets-lte3/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">
    <!-- Select2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/select2/css/select2.min.css">
    <link rel="stylesheet"
        href="<?=base_url()?>assets-lte3/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- SweetAlert2 -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <!-- Toastr -->
    <link rel="stylesheet" href="<?=base_url()?>assets-lte3/plugins/toastr/toastr.min.css">

    <!-- SCRIPT -->
    <!-- jQuery -->
    <script src="<?=base_url()?>assets-lte3/plugins/jquery/jquery.min.js"></script>

    <!-- DataTables  & Plugins -->
    <script src="<?=base_url()?>assets-lte3/plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>

    <!-- InputMask -->
    <script src="<?=base_url()?>assets-lte3/plugins/moment/moment.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/inputmask/jquery.inputmask.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="<?=base_url()?>assets-lte3/plugins/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <script>
    $.widget.bridge('uibutton', $.ui.button)
    </script>
    <!-- Bootstrap 4 -->
    <script src="<?=base_url()?>assets-lte3/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- ChartJS -->
    <script src="<?=base_url()?>assets-lte3/plugins/chart.js/Chart.min.js"></script>
    <!-- Sparkline -->
    <script src="<?=base_url()?>assets-lte3/plugins/sparklines/sparkline.js"></script>
    <!-- JQVMap -->
    <!-- <script src="<?=base_url()?>assets-lte3/plugins/jqvmap/jquery.vmap.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
    <!-- jQuery Knob Chart -->
    <script src="<?=base_url()?>assets-lte3/plugins/jquery-knob/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="<?=base_url()?>assets-lte3/plugins/moment/moment.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- Tempusdominus Bootstrap 4 -->
    <script src="<?=base_url()?>assets-lte3/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js">
    </script>
    <!-- Summernote -->
    <script src="<?=base_url()?>assets-lte3/plugins/summernote/summernote-bs4.min.js"></script>
    <!-- overlayScrollbars -->
    <script src="<?=base_url()?>assets-lte3/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?=base_url()?>assets-lte3/dist/js/adminlte.js"></script>

    <!-- chart.js -->
    <script src="<?=base_url()?>assets-lte3/plugins/chart.js/Chart.min.js"></script>
    <!-- validate -->
    <script src="<?=base_url()?>assets-lte3/plugins/jquery-validation/jquery.validate.min.js"></script>
    <script src="<?=base_url()?>assets-lte3/plugins/jquery-validation/additional-methods.js"></script>
    <!-- Select2 -->
    <script src="<?=base_url()?>assets-lte3/plugins/select2/js/select2.full.min.js"></script>

    <!-- autoNumeric -->

    <script src="<?=base_url()?>assets-lte3/plugins/autoNumeric4/dist/autoNumeric.min.js"></script>



    <!-- bs-custom-file-input -->
    <script src="<?=base_url()?>assets-lte3/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

    <!-- sorting date datatable -->
    <script src="<?=base_url()?>assets-lte3/plugins/sorting/datetime-moment.js"></script>
    <!-- SweetAlert2 -->
    <script src="<?=base_url()?>assets-lte3/plugins/sweetalert2/sweetalert2.min.js"></script>
    <!-- Toastr -->
    <script src="<?=base_url()?>assets-lte3/plugins/toastr/toastr.min.js"></script>



</head>

<body class="hold-transition sidebar-mini layout-fixed">
    <div class="wrapper">

        <!-- Navbar -->
        <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <!-- <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?= base_url() ?>"
                        class="nav-link <?php echo ($nav_status == 'dashboard') ? 'active':'';?>">Dashboard</a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="<?= base_url() ?>diklat"
                        class="nav-link <?php echo ($nav_status == 'diklat') ? 'active':'';?>">Data Diklat</a>
                </li>
            </ul> -->

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                <!-- Notifications Dropdown Menu -->
                <li class="nav-item dropdown">
                    <a class="nav-link" data-toggle="dropdown" href="#">
                        <i class="fas fa-cog"></i>
                    </a>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                        <div class="dropdown-divider"></div>
                        <div class="media">
                            <img src="<?= base_url() ?>assets-lte3/img/user.png" alt="User Avatar"
                                class="img-size-50 mr-3 img-circle">
                            <div class="media-body">
                                <h3 class="dropdown-item-title">
                                    <?=$this->session->userdata('nama')?>
                                </h3>
                            </div>
                        </div>
                        <div class="dropdown-divider"></div>
                        <a href="<?= base_url() ?>user" class="dropdown-item">
                            <i class="fas fa-user-alt"></i> Profile
                        </a>
                        <div class="dropdown-divider"></div>
                        <a href="<?= base_url() ?>login/logout" class="dropdown-item">
                            <i class="fas fa-power-off"></i> Log Out
                        </a>
                    </div>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->

            <a href="<?= base_url() ?>" class="brand-link">
                <img src="<?=base_url()?>assets-lte3/img/icon128x128.png" alt="AdminLTE Logo"
                    class="brand-image img-circle elevation-3" style="opacity: .8">
                <span class="brand-text font-weight-light">MonoKOMP</span>
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu"
                        data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->

                        <?php if($this->session->userdata('role')==0 || $this->session->userdata('role')==2 || $this->session->userdata('role')==3
                        || $this->session->userdata('role')==4){?>

                        <li class="nav-item <?php echo ($nav_tree == 'diklat') ? 'menu-is-opening menu-open':'';?>">
                            <a href="#" class="nav-link <?php echo ($nav_tree == 'diklat') ? 'active':'';?>">
                                <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Monitoring Diklat/PPM
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">

                                    <a href="<?= base_url() ?>"
                                        class="nav-link <?php echo ($nav_status == 'dashboard') ? 'active':'';?>">
                                        <i class="nav-icon fas fa-chart-pie"></i>

                                        <p>Dashboard</p>
                                    </a>

                                </li>
                                <li class="nav-item">
                                    <a href="<?= base_url() ?>diklat"
                                        class="nav-link <?php echo ($nav_status == 'diklat') ? 'active':'';?>">
                                        <i class="nav-icon fas fa-th"></i>
                                        <p>
                                            Data Diklat
                                        </p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php } ?>

                        <?php if($this->session->userdata('role')==2 || $this->session->userdata('role')==1 || $this->session->userdata('role')==3 || $this->session->userdata('role')==4){?>

                        <li class="nav-item <?php echo ($nav_tree == 'pengadaan') ? 'menu-is-opening menu-open':'';?>">
                            <a href="#" class="nav-link <?php echo ($nav_tree == 'pengadaan') ? 'active':'';?>">
                                <i class="nav-icon nav-icon fas fa-book"></i>
                                <p>
                                    Monitoring ND
                                    <i class="right fas fa-angle-left"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                <li class="nav-item">

                                    <a href="<?= base_url() ?>pengadaan"
                                        class="nav-link <?php echo ($nav_status == 'nd_pengadaan') ? 'active':'';?>">
                                        <i class="nav-icon fas fa-table"></i>

                                        <p>Daftar ND</p>
                                    </a>

                                </li>

                            </ul>
                        </li>

                        <?php } ?>

                        <li class="nav-item">
                            <a href="<?= base_url() ?>user"
                                class="nav-link <?php echo ($nav_status == 'user') ? 'active':'';?>">
                                <i class="fas fa-user-alt"></i>
                                <p>
                                    Profile
                                </p>
                            </a>
                        </li>


                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <!-- /.content-header -->
            <!-- Main content -->
            <section class="content">
                <div class="container-fluid">

                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0"><?= $judul ?></h1>
                        </div><!-- /.col -->
                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                                <li class="breadcrumb-item"><a href="<?= base_url() ?><?=$subjudul?>"><?= $judul ?></a>
                                </li>
                                <!-- <li class="breadcrumb-item active">Dashboard v3</li> -->
                            </ol>
                        </div><!-- /.col -->
                    </div>
                    <div class="row text-center">
                        <div class="col-lg-12">
                            <div class="card card-dark card-outline">
                                <!-- <div class="card"> -->